/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.util.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXB;

import net.automatalib.automata.concepts.StateIDs;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.FastMealy;
import net.automatalib.automata.transout.impl.FastMealyState;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.SimpleAlphabet;

public class MealyXMLSerializer {
	
	public static FastMealy<String, String> loadFromXML(File automatonFile){
		if (!automatonFile.exists()) {
			System.err.println("The given automata file does not exist!" + automatonFile);
		}

		try(Reader reader = new BufferedReader(new InputStreamReader(new FileInputStream(automatonFile)))) {
			if (automatonFile.getName().endsWith(".xml")) {
				// load from xml and create automat
				FastMealy<String, String> automaton = fromXml(reader);
				System.err.println("Automaton has " + automaton.size() + " states");
			
				return automaton;
			} 
			throw new IllegalArgumentException("Specified File is no xml file");

		} catch (FileNotFoundException e) {
			System.err.println("file not found");
		} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static <S,T,I,O> void toXml(MealyMachine<S, I, T, O> mealy, Collection<I> inputAlphabet, Writer w) {
		AutomatonType aut = new AutomatonType();
		aut.setVersion("1.0");
		aut.setHelper("de.ls5.jlearn.util.xml.SimpleXmlSerializationHelper");
		List<StateType> states = aut.getState();
		List<TransitionType> transitions = aut.getTransition();
		
		AlphabetType alph = new AlphabetType();
		aut.setAlphabet(alph);
		List<String> alphabet = alph.getInput();
		
		Set<String> inputSet = new HashSet<>();
		for(I input : inputAlphabet) {
			alphabet.add(input.toString());
			inputSet.add(input.toString());
		}
		
		StateIDs<S> ids = mealy.stateIDs();
		
		for(S s : mealy) {
			StateType st = new StateType();
			st.setId(Integer.toString(ids.getStateId(s)));
			if(mealy.getInitialState() == s)
				st.setInitial(true);
			
			for(I input : inputAlphabet) {
				T trans = mealy.getTransition(s, input);
				if(trans == null)
					continue;
				O output = mealy.getTransitionOutput(trans);
				
				TransitionType tt = new TransitionType();
				tt.setInput(input.toString());
				if(inputSet.add(input.toString()))
					alphabet.add(input.toString());
				tt.setOutput(output.toString());
				tt.setSource(Integer.toString(ids.getStateId(s)));
				S target = mealy.getSuccessor(trans);
				tt.setDest(Integer.toString(ids.getStateId(target)));
				
				transitions.add(tt);
			}
			
			states.add(st);
		}
		
		JAXB.marshal(aut, w);
	}
	
	public static FastMealy<String,String> fromXml(Reader r) {
		AutomatonType aut = JAXB.unmarshal(r, AutomatonType.class);
		
		Alphabet<String> inputs = new SimpleAlphabet<>();
		
		AlphabetType alph = aut.getAlphabet();
		if(alph != null) {
			for(String input : alph.getInput())
				inputs.add(input);
		}
		for(TransitionType tt : aut.getTransition())
			inputs.add(tt.getInput());
		
		FastMealy<String,String> mealy = new FastMealy<>(inputs);
		
		Map<String,FastMealyState<String>> stateMap = new HashMap<>();
		
		for(StateType st : aut.getState()) {
			FastMealyState<String> s = null;
			if(st.isInitial() != null && st.isInitial())
				s = mealy.addInitialState();
			else
				s = mealy.addState();
			stateMap.put(st.getId(), s);
		}
		
		for(TransitionType tt : aut.getTransition()) {
			FastMealyState<String> src = stateMap.get(tt.getSource());
			FastMealyState<String> tgt = stateMap.get(tt.getDest());
			
			String input = tt.getInput();
			String output = tt.getOutput();
			
			mealy.addTransition(src, input, tgt, output);
		}
		
		return mealy;
	}
}
