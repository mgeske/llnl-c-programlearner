/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.util;

import com.google.code.mathparser.MathParser;
import com.google.code.mathparser.MathParserFactory;
import com.google.code.mathparser.parser.calculation.Result;


public final class Util {

    private static final String VARIABLE_NAME = "n";

    public static long calcExpression(String expr, int n) {
        MathParser parser = MathParserFactory.create();
        Result result;
        String repExpr;
        repExpr = expr.replaceAll(VARIABLE_NAME, Integer.toString(n));
        result = parser.calculate(repExpr);
        return result.doubleValue().longValue();
    }
}
