/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.learner;

import java.util.Collection;

import javax.xml.namespace.QName;

import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

public class KVMealyBuilderWrapper<I, O> implements MealyLearnerBuilder<I, O, KearnsVaziraniMealy<I, O>> {

    private net.automatalib.words.Alphabet<I> alphabet;

    private de.learnlib.api.MembershipOracle<I, net.automatalib.words.Word<O>> oracle;

    private boolean repeatedCounterexampleEvaluation;

    public KVMealyBuilderWrapper() {
        this.repeatedCounterexampleEvaluation = true;
    }

    public static <I, O> KVMealyBuilderWrapper<I, O> fromXmlElement(de.learnlib.util.xml.LearnerType xmlType, Collection<I> alphabet) {
        KVMealyBuilderWrapper<I, O> learner = new KVMealyBuilderWrapper<>();

        String repeatedCounterexampleEvaluation = xmlType.getOtherAttributes().get(new QName("repeatedCounterexampleEvaluation"));
        if (repeatedCounterexampleEvaluation != null) {
                    learner.setRepeatedCounterexampleEvaluation(Boolean.getBoolean(repeatedCounterexampleEvaluation));        
        }

        return learner;
    }
    
    @Override
    public KearnsVaziraniMealy<I, O> create() {
        return new KearnsVaziraniMealy<I,O>(alphabet, oracle,
                repeatedCounterexampleEvaluation, null);
    }

    public Alphabet<I> getAlphabet() {
        return this.alphabet;
    }

    @Override
    public void setAlphabet(Alphabet<I> alphabet) {
        this.alphabet = alphabet;
    }

    public KVMealyBuilderWrapper<I, O> withAlphabet(Alphabet<I> alphabet) {
        this.alphabet = alphabet;
        return this;
    }

    public de.learnlib.api.MembershipOracle<I, Word<O>> getOracle() {
        return this.oracle;
    }

    @Override
    public void setOracle(de.learnlib.api.MembershipOracle<I, Word<O>> oracle) {
        this.oracle = oracle;
    }

    public KVMealyBuilderWrapper<I, O> withOracle(
            de.learnlib.api.MembershipOracle<I, Word<O>> oracle) {
        this.oracle = oracle;
        return this;
    }

    public boolean getRepeatedCounterexampleEvaluation() {
        return this.repeatedCounterexampleEvaluation;
    }

    public void setRepeatedCounterexampleEvaluation(boolean repeatedCounterexampleEvaluation) {
        this.repeatedCounterexampleEvaluation = repeatedCounterexampleEvaluation;
    }

    public KVMealyBuilderWrapper<I, O> withRepeatedCounterexampleEvaluation(boolean repeatedCounterexampleEvaluation) {
        this.repeatedCounterexampleEvaluation = repeatedCounterexampleEvaluation;
        return this;
    }
}
