/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.learner;

import java.util.Collection;

import javax.xml.namespace.QName;

import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.MembershipOracle;
import de.learnlib.counterexamples.LocalSuffixFinder;
import de.learnlib.counterexamples.LocalSuffixFinders;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

public class TTTMealyBuilderWrapper<I, O>
		implements MealyLearnerBuilder<I, O, TTTLearnerMealy<I, O>> {

	private Alphabet<I> alphabet;

	private de.learnlib.api.MembershipOracle<I, Word<O>> oracle;

	private LocalSuffixFinder analyzer;

	@SuppressWarnings("unchecked")
	public TTTMealyBuilderWrapper() {
		this.analyzer = (LocalSuffixFinder<I, O>) de.learnlib.algorithms.ttt.base.BaseTTTLearner.BuilderDefaults
				.suffixFinder();
	}

	@Override
	public de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy<I, O> create() {
		return new TTTLearnerMealy<>(alphabet, oracle, analyzer);
	}

	public static <I, O> TTTMealyBuilderWrapper<I, O> fromXmlElement(
			de.learnlib.util.xml.LearnerType xmlType, Collection<I> alphabet) {
		TTTMealyBuilderWrapper<I, O> learner = new TTTMealyBuilderWrapper<>();

		String finder = xmlType.getOtherAttributes()
				.get(new QName("localSuffixFinder"));
		if (finder != null) {
			for (LocalSuffixFinder suffixFinder : LocalSuffixFinders.values()) {
				if (suffixFinder.toString().equals(finder)) {
					learner.setSuffixFinder(suffixFinder);
				}
			}
		}

		return learner;
	}

	public Alphabet<I> getAlphabet() {
		return this.alphabet;
	}

	@Override
	public void setAlphabet(Alphabet<I> alphabet) {
		this.alphabet = alphabet;
	}

	public TTTMealyBuilderWrapper<I, O> withAlphabet(Alphabet<I> alphabet) {
		this.alphabet = alphabet;
		return this;
	}

	public MembershipOracle<I, Word<O>> getOracle() {
		return this.oracle;
	}

	@Override
	public void setOracle(MembershipOracle<I, Word<O>> oracle) {
		this.oracle = oracle;
	}

	public TTTMealyBuilderWrapper<I, O> withOracle(
			de.learnlib.api.MembershipOracle<I, Word<O>> oracle) {
		this.oracle = oracle;
		return this;
	}

	public LocalSuffixFinder<I, O> getSuffixFinder() {
		return this.analyzer;
	}

	public void setSuffixFinder(LocalSuffixFinder<I, O> suffixFinder) {
		this.analyzer = suffixFinder;
	}

	public TTTMealyBuilderWrapper<I, O> withSuffixFinder(
			LocalSuffixFinder<I, O> suffixFinder) {
		this.analyzer = suffixFinder;
		return this;
	}
}
