/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.mqoracle;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import net.automatalib.commons.util.comparison.CmpUtil;
import net.automatalib.commons.util.mappings.Mapping;
import net.automatalib.commons.util.mappings.Mappings;
import net.automatalib.incremental.mealy.IncrementalMealyBuilder;
import net.automatalib.incremental.mealy.dag.IncrementalMealyDAGBuilder;
import net.automatalib.incremental.mealy.tree.IncrementalMealyTreeBuilder;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.Query;
import de.learnlib.cache.LearningCacheOracle.MealyLearningCacheOracle;
import de.learnlib.cache.mealy.MealyCacheConsistencyTest;
import de.learnlib.cache.mealy.MealyCacheOracle;
import de.learnlib.oracles.AbstractQuery;

public class MealyCacheOracleWrapper<I, O> implements MealyOracleWrapperInterface<I, O>, MealyLearningCacheOracle<I, O> {

    private MembershipOracle<I, Word<O>> delegate;

    private IncrementalMealyBuilder<I, O> incMealy;

    private final Lock incMealyLock;

    private Comparator<? super Query<I, ?>> queryCmp;

    private Mapping<? super O, ? extends O> errorSyms;

    public MealyCacheOracleWrapper() {
        this.incMealyLock = new ReentrantLock();
    }

    public MealyCacheOracleWrapper(IncrementalMealyBuilder<I, O> incrementalBuilder) {
        this(incrementalBuilder, new ReentrantLock());
    }

    private MealyCacheOracleWrapper(IncrementalMealyBuilder<I, O> incrementalBuilder, Lock lock) {
        this.incMealy = incrementalBuilder;
        this.incMealyLock = lock;
        this.queryCmp = new ReverseLexCmp<>(incrementalBuilder.getInputAlphabet());
    }

    @Override
    public MealyCacheOracleWrapper<I, O> setPrefixClosureMapping(Map<O, O> prefixClosureMapping) {
        if (prefixClosureMapping != null) {
            errorSyms = Mappings.fromMap(prefixClosureMapping);
        }
        return this;
    }

    @Override
    public MealyOracleWrapperInterface<I, O> setAlphabet(Alphabet<I> inputAlphabet) {
        if (incMealy == null) {
            incMealy = new IncrementalMealyDAGBuilder<>(inputAlphabet);
            this.queryCmp = new ReverseLexCmp<>(incMealy.getInputAlphabet());
        }
        return this;
    }

    @Override
    public MealyOracleWrapperInterface<I, O> setNextOracle(MembershipOracle<I, Word<O>> nextOracle) {
        this.delegate = nextOracle;
        return this;
    }

    @Override
    public MealyCacheOracleWrapper<I, O> create() {
        checkNotNull(delegate);
        checkNotNull(incMealy);
        checkNotNull(incMealyLock);
        checkNotNull(queryCmp);
        return this;
    }

    private static final class ReverseLexCmp<I> implements Comparator<Query<I, ?>> {

        private final Alphabet<I> alphabet;

        public ReverseLexCmp(Alphabet<I> alphabet) {
            this.alphabet = alphabet;
        }

        @Override
        public int compare(Query<I, ?> o1, Query<I, ?> o2) {
            return -CmpUtil.lexCompare(o1.getInput(), o2.getInput(), alphabet);
        }
    }

    public static <I, O> MealyCacheOracle<I, O> createDAGCacheOracle(Alphabet<I> inputAlphabet,
            MembershipOracle<I, Word<O>> delegate) {
        return createDAGCacheOracle(inputAlphabet, null, delegate);
    }

    public static <I, O> MealyCacheOracle<I, O> createDAGCacheOracle(Alphabet<I> inputAlphabet,
            Mapping<? super O, ? extends O> errorSyms, MembershipOracle<I, Word<O>> delegate) {
        IncrementalMealyBuilder<I, O> incrementalBuilder = new IncrementalMealyDAGBuilder<>(inputAlphabet);
        return new MealyCacheOracle<>(incrementalBuilder, errorSyms, delegate);
    }

    public static <I, O> MealyCacheOracle<I, O> createTreeCacheOracle(Alphabet<I> inputAlphabet,
            MembershipOracle<I, Word<O>> delegate) {
        return createTreeCacheOracle(inputAlphabet, null, delegate);
    }

    public static <I, O> MealyCacheOracle<I, O> createTreeCacheOracle(Alphabet<I> inputAlphabet,
            Mapping<? super O, ? extends O> errorSyms, MembershipOracle<I, Word<O>> delegate) {
        IncrementalMealyBuilder<I, O> incrementalBuilder = new IncrementalMealyTreeBuilder<>(inputAlphabet);
        return new MealyCacheOracle<>(incrementalBuilder, errorSyms, delegate);
    }

    public int getCacheSize() {
        return incMealy.asGraph().size();
    }

    /*
     * (non-Javadoc)
     * @see de.learnlib.cache.LearningCache#createCacheConsistencyTest()
     */
    @Override
    public MealyCacheConsistencyTest<I, O> createCacheConsistencyTest() {
        return new MealyCacheConsistencyTest<>(incMealy, incMealyLock);
    }

    /*
     * (non-Javadoc)
     * @see de.learnlib.api.MembershipOracle#processQueries(java.util.Collection)
     */
    @Override
    public void processQueries(Collection<? extends Query<I, Word<O>>> queries) {
        if (queries.isEmpty()) {
            return;
        }

        List<Query<I, Word<O>>> qrys = new ArrayList<>(queries);
        Collections.sort(qrys, queryCmp);

        List<MasterQuery<I, O>> masterQueries = new ArrayList<>();

        Iterator<Query<I, Word<O>>> it = qrys.iterator();
        Query<I, Word<O>> q = it.next();
        Word<I> ref = q.getInput();

        incMealyLock.lock();
        try {
            MasterQuery<I, O> master = createMasterQuery(ref);
            if (!master.isAnswered()) {
                masterQueries.add(master);
            }
            master.addSlave(q);

            while (it.hasNext()) {
                q = it.next();
                Word<I> curr = q.getInput();
                if (!curr.isPrefixOf(ref)) {
                    master = createMasterQuery(curr);
                    if (!master.isAnswered()) {
                        masterQueries.add(master);
                    }
                }

                master.addSlave(q);
                // Update ref to increase the effectivity of the length check in
                // isPrefixOf
                ref = curr;
            }
        }
        finally {
            incMealyLock.unlock();
        }

        delegate.processQueries(masterQueries);

        incMealyLock.lock();
        try {
            for (MasterQuery<I, O> m : masterQueries) {
                postProcess(m);
            }
        }
        finally {
            incMealyLock.unlock();
        }
    }

    private void postProcess(MasterQuery<I, O> master) {
        Word<I> word = master.getSuffix();
        Word<O> answer = master.getAnswer();

        if (errorSyms == null) {
            incMealy.insert(word, answer);
            return;
        }

        int answLen = answer.length();
        int i = 0;
        while (i < answLen) {
            O sym = answer.getSymbol(i++);
            if (errorSyms.get(sym) != null)
                break;
        }

        if (i == answLen) {
            incMealy.insert(word, answer);
        }
        else {
            incMealy.insert(word.prefix(i), answer.prefix(i));
        }
    }

    private MasterQuery<I, O> createMasterQuery(Word<I> word) {
        WordBuilder<O> wb = new WordBuilder<>();
        if (incMealy.lookup(word, wb)) {
            return new MasterQuery<>(word, wb.toWord());
        }

        if (errorSyms == null) {
            return new MasterQuery<>(word);
        }
        int wbSize = wb.size();
        O repSym;
        if (wbSize == 0 || (repSym = errorSyms.get(wb.getSymbol(wbSize - 1))) == null) {
            return new MasterQuery<>(word, errorSyms);
        }

        wb.repeatAppend(word.length() - wbSize, repSym);
        return new MasterQuery<>(word, wb.toWord());
    }

    final static class MasterQuery<I, O> extends AbstractQuery<I, Word<O>> {

        private Word<O> answer;

        private final Mapping<? super O, ? extends O> errorSyms;

        private final List<Query<I, Word<O>>> slaves;

        public MasterQuery(Word<I> word) {
            this(word, (Mapping<? super O, ? extends O>) null);
        }

        public MasterQuery(Word<I> word, Word<O> output) {
            super(word);
            this.answer = output;
            this.errorSyms = null;
            this.slaves = null;
        }

        public MasterQuery(Word<I> word, Mapping<? super O, ? extends O> errorSyms) {
            super(word);
            this.errorSyms = errorSyms;
            this.slaves = new ArrayList<>();
        }

        public Word<O> getAnswer() {
            return answer;
        }

        public boolean isAnswered() {
            return (answer != null);
        }

        /*
         * (non-Javadoc)
         * @see de.learnlib.api.Query#answer(java.lang.Object)
         */
        @Override
        public void answer(Word<O> output) {
            output = truncateOutput(output);
            this.answer = output;
            for (Query<I, Word<O>> slave : slaves) {
                answerSlave(slave);
            }
        }

        public void addSlave(Query<I, Word<O>> slave) {
            if (slaves == null) {
                answerSlave(slave);
            }
            else {
                slaves.add(slave);
            }
        }

        private void answerSlave(Query<I, Word<O>> slave) {
            int start = slave.getPrefix().length();
            int end = start + slave.getSuffix().length();
            slave.answer(answer.subWord(start, end));
        }

        private Word<O> truncateOutput(Word<O> output) {
            if (errorSyms == null) {
                return output;
            }

            int maxLen = output.length() - 1;
            int i = 0;
            O repSym = null;

            while (i < maxLen && repSym == null) {
                O sym = output.getSymbol(i++);
                repSym = errorSyms.get(sym);
            }

            if (repSym == null) {
                return output;
            }

            WordBuilder<O> wb = new WordBuilder<>(maxLen + 1);
            wb.append(output.prefix(i));
            wb.repeatAppend(1 + maxLen - i, repSym);

            return wb.toWord();
        }

        /**
         * @see de.learnlib.oracles.AbstractQuery#toStringWithAnswer(Object)
         */
        @Override
        public String toString() {
            return toStringWithAnswer(answer);
        }

    }
}