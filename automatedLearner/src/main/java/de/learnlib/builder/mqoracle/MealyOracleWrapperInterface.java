/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.mqoracle;

import java.util.Map;

import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.MembershipOracle.MealyMembershipOracle;

public interface MealyOracleWrapperInterface<I, O> {

    public MealyOracleWrapperInterface<I, O> setPrefixClosureMapping(Map<O, O> prefixClosureMapping);

    public MealyOracleWrapperInterface<I, O> setNextOracle(MembershipOracle<I, Word<O>> nextOracle);

    public MealyOracleWrapperInterface<I, O> setAlphabet(Alphabet<I> inputAlphabet);

    public MealyMembershipOracle<I, O> create();
}
