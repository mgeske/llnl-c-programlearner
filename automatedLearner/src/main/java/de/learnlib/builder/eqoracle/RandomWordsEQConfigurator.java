/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.eqoracle;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.EquivalenceOracle.MealyEquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.SUL;
import de.learnlib.eqtests.basic.RandomWordsEQOracle.MealyRandomWordsEQOracle;
import de.learnlib.util.Util;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

import javax.xml.namespace.QName;

import de.learnlib.util.xml.EqOracleType;

import java.util.Random;

public class RandomWordsEQConfigurator<I, O> implements EQConfigurator<I, O> {

	public static <I, O> RandomWordsEQConfigurator<I, O> fromXmlElement(
			final EqOracleType oracleConfig) {
		String minLengthExpr = oracleConfig.getOtherAttributes().get(
				new QName("minLengthExpr"));
		if (minLengthExpr == null)
			minLengthExpr = "1";

		String maxLengthExpr = oracleConfig.getOtherAttributes().get(
				new QName("maxLengthExpr"));
		if (maxLengthExpr == null)
			maxLengthExpr = "20";

		Integer maxTests = null;
		Long randomSeed = null;
		try {
			final QName RANDOM_SEEDS_QNAME = new QName("randomSeed");
			if (oracleConfig.getOtherAttributes().containsKey(
					RANDOM_SEEDS_QNAME)) {
				randomSeed = Long.decode(oracleConfig.getOtherAttributes().get(
						RANDOM_SEEDS_QNAME));
			}
			maxTests = Integer.parseInt(oracleConfig.getOtherAttributes().get(
					new QName("maxTests")));
		} catch (final NumberFormatException e) {
			//
		}
		if (maxTests == null)
			maxTests = 30;

		return new RandomWordsEQConfigurator<>(minLengthExpr, maxLengthExpr,
				maxTests, randomSeed);
	}

	private MembershipOracle<I, Word<O>> mqOracle;

	private final String minLengthExpr;

	private final String maxLengthExpr;

	private final int maxTests;

	private final Random random;

	public RandomWordsEQConfigurator(final String minLengthExpr,
			final String maxLengthExpr, final int maxTests,
			final Long randomSeed) {
		super();
		this.minLengthExpr = minLengthExpr;
		this.maxLengthExpr = maxLengthExpr;
		this.maxTests = maxTests;
		this.random = (randomSeed == null) ? new Random() : new Random(
				randomSeed);
	}

	@Override
	public <S, T> MealyEquivalenceOracle<I, O> create(
			final MealyMachine<S, I, T, O> hypothesis) {
		return new MealyRandomWordsEQOracle<>(mqOracle,
				(int) Util.calcExpression(minLengthExpr, hypothesis.size()),
				(int) Util.calcExpression(maxLengthExpr, hypothesis.size()),
				maxTests, random);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<? extends MealyEquivalenceOracle<I, O>> getOracleClass() {
		return (Class<? extends EquivalenceOracle.MealyEquivalenceOracle<I, O>>) ((Class<?>) MealyRandomWordsEQOracle.class);
	}

	@Override
	public void setAlphabet(final Alphabet<I> alphabet) {
		//
	}

	@Override
	public void setSUL(final SUL<I, O> sul,
			final MembershipOracle<I, Word<O>> sulOracle) {
		this.mqOracle = sulOracle;
	}
}
