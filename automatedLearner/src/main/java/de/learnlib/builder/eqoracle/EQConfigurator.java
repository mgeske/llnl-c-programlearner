/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.eqoracle;

import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.SUL;

public interface EQConfigurator<I, O> {

    public <S, T> EquivalenceOracle.MealyEquivalenceOracle<I, O> create(MealyMachine<S, I, T, O> hypothesis);

    public Class<? extends EquivalenceOracle.MealyEquivalenceOracle<I, O>> getOracleClass();

    public void setAlphabet(Alphabet<I> alphabet);

    public void setSUL(SUL<I, O> sul, de.learnlib.api.MembershipOracle<I, Word<O>> sulOracle);
}
