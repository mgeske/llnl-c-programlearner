/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.eqoracle;

import java.util.Random;

import javax.xml.namespace.QName;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.SUL;
import de.learnlib.eqtests.basic.mealy.RandomWalkEQOracle;
import de.learnlib.util.Util;
import de.learnlib.util.xml.EqOracleType;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

public class RandomWalkEQConfigurator<I, O> implements EQConfigurator<I, O> {

	public static <I, O> RandomWalkEQConfigurator<I, O> fromXmlElement(
			final EqOracleType oracleConfig) {

		String maxStepsExpr = oracleConfig.getOtherAttributes().get(
				new QName("maxLengthExpr"));
		if (maxStepsExpr == null)
			maxStepsExpr = "20";

		Boolean resetStepCount = null;
		Long randomSeed = null;
		Double restartProb = null;
		try {
			final QName RANDOM_SEEDS_QNAME = new QName("randomSeed");
			if (oracleConfig.getOtherAttributes().containsKey(
					RANDOM_SEEDS_QNAME)) {
				randomSeed = Long.decode(oracleConfig.getOtherAttributes().get(
						RANDOM_SEEDS_QNAME));
			}
			resetStepCount = Boolean.parseBoolean(oracleConfig
					.getOtherAttributes().get(new QName("maxTests")));
			restartProb = Double.parseDouble(oracleConfig.getOtherAttributes()
					.get(new QName("restartProbability")));

		} catch (final NumberFormatException e) {
			//
		}
		if (resetStepCount == null)
			resetStepCount = false;
		if (restartProb == null)
			restartProb = 0.5;

		return new RandomWalkEQConfigurator<>(restartProb, maxStepsExpr,
				resetStepCount, randomSeed);
	}

	private final Random random;

	private final double restartProbability;

	private final String maxStepsExpr;

	private final boolean resetStepCount;

	private SUL<I, O> sul;

	public RandomWalkEQConfigurator(final double restartProbability,
			final String maxStepsExpr, final boolean resetStepCount,
			final Long randomSeed) {
		super();
		this.restartProbability = restartProbability;
		this.maxStepsExpr = maxStepsExpr;  
		this.resetStepCount = resetStepCount;
		this.random = (randomSeed == null) ? new Random() : new Random(
				randomSeed);
	}

	@Override
	public <S, T> EquivalenceOracle.MealyEquivalenceOracle<I, O> create(
			final MealyMachine<S, I, T, O> hypothesis) {
		int size = 0;
		if(hypothesis.size() < 3)
			size = 20;
		else 
			size = hypothesis.size();
			
		return new RandomWalkEQOracle<>(restartProbability,
				Util.calcExpression(maxStepsExpr, size),
				resetStepCount, random, sul);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<? extends EquivalenceOracle.MealyEquivalenceOracle<I, O>> getOracleClass() {
		return (Class<? extends EquivalenceOracle.MealyEquivalenceOracle<I, O>>) ((Class<?>) RandomWalkEQOracle.class);
	}

	@Override
	public void setAlphabet(final Alphabet<I> alphabet) {
		//
	}

	@Override
	public void setSUL(final SUL<I, O> sul,
			final MembershipOracle<I, Word<O>> sulOracle) {
		this.sul = sul;
	}
}
