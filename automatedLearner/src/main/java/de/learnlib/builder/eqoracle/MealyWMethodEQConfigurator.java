/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.builder.eqoracle;

import javax.xml.namespace.QName;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.EquivalenceOracle.MealyEquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.api.SUL;
import de.learnlib.eqtests.basic.WMethodEQOracle.MealyWMethodEQOracle;
import de.learnlib.util.Util;
import de.learnlib.util.xml.EqOracleType;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

public class MealyWMethodEQConfigurator<I, O> implements EQConfigurator<I, O> {

    public static <I, O> MealyWMethodEQConfigurator<I, O> fromXmlElement(final EqOracleType oracleConfig) {
        String maxDepthExpr = oracleConfig.getOtherAttributes().get(new QName("maxDepth"));
        if (maxDepthExpr == null) {
            maxDepthExpr = "1";
        }
        return new MealyWMethodEQConfigurator<>(maxDepthExpr);
    }

    private final String maxDepthExpr;

    private MembershipOracle<I, Word<O>> sulOracle;

    public MealyWMethodEQConfigurator(final String maxDepthExpr) {
        super();
        this.maxDepthExpr = maxDepthExpr;
    }

    @Override
    public <S, T> MealyEquivalenceOracle<I, O> create(final MealyMachine<S, I, T, O> hypothesis) {
        return new MealyWMethodEQOracle<>((int) Util.calcExpression(maxDepthExpr, hypothesis.size()), sulOracle);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Class<? extends MealyEquivalenceOracle<I, O>> getOracleClass() {
        return (Class<? extends EquivalenceOracle.MealyEquivalenceOracle<I, O>>) ((Class<?>) MealyWMethodEQOracle.class);
    }

    @Override
    public void setAlphabet(final Alphabet<I> alphabet) {
        //
    }

    @Override
    public void setSUL(final SUL<I, O> sul, final MembershipOracle<I, Word<O>> sulOracle) {
        this.sulOracle = sulOracle;
    }

}
