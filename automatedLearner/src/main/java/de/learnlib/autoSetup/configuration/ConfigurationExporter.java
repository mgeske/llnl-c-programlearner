/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.autoSetup.configuration;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXB;
import javax.xml.namespace.QName;

import de.learnlib.util.xml.Entry;
import de.learnlib.util.xml.EqOracleType;
import de.learnlib.util.xml.EqOraclesType;
import de.learnlib.util.xml.LearnSetup;
import de.learnlib.util.xml.LearnerType;
import de.learnlib.util.xml.MqOracleType;
import de.learnlib.util.xml.MqOraclesType;
import de.learnlib.util.xml.PrefixClosureMapping;

import de.learnlib.autoSetup.ConfigurableActiveLearningSetup.MembershipOracle;
import de.learnlib.builder.eqoracle.IncrementalWMethodEQConfigurator;
import de.learnlib.builder.eqoracle.MealyWMethodEQConfigurator;
import de.learnlib.builder.eqoracle.RandomWalkEQConfigurator;
import de.learnlib.builder.eqoracle.RandomWordsEQConfigurator;
import de.learnlib.builder.learner.DTMealyBuilderWrapper;
import de.learnlib.builder.learner.KVMealyBuilderWrapper;
import de.learnlib.builder.learner.TTTMealyBuilderWrapper;
import de.learnlib.counterexamples.LocalSuffixFinders;

public class ConfigurationExporter {

	public static EqOracleType createIWMO() {
		EqOracleType t = new EqOracleType();
		t.setClazz(IncrementalWMethodEQConfigurator.class.getCanonicalName());
		Map<QName, String> otherAttributes = t.getOtherAttributes();
		otherAttributes.put(new QName("maxDepth"), "2");
		return t;
	}

	public static EqOracleType createRWordsO() {
		EqOracleType t = new EqOracleType();
		t.setClazz(RandomWordsEQConfigurator.class.getCanonicalName());
		Map<QName, String> otherAttributes = t.getOtherAttributes();
		otherAttributes.put(new QName("minLengthExpr"), "n");
		otherAttributes.put(new QName("maxLengthExpr"), "4*n");
		otherAttributes.put(new QName("maxTests"), "30");
		otherAttributes.put(new QName("randomSeed"), "5000");
		return t;
	}

	public static EqOracleType createWMO() {
		EqOracleType t = new EqOracleType();
		t.setClazz(MealyWMethodEQConfigurator.class.getCanonicalName());
		Map<QName, String> otherAttributes = t.getOtherAttributes();
		otherAttributes.put(new QName("maxDepthExpr"), "2");
		return t;
	}

	public static EqOracleType createRWalkO() {
		EqOracleType t = new EqOracleType();
		t.setClazz(RandomWalkEQConfigurator.class.getCanonicalName());
		Map<QName, String> otherAttributes = t.getOtherAttributes();
		otherAttributes.put(new QName("maxStepsExpr"), "4*n");
		otherAttributes.put(new QName("restartProbability"), "0.5"); // double
		otherAttributes.put(new QName("resetStepCount"),
				Boolean.toString(false));
		otherAttributes.put(new QName("randomSeed"), "5000"); // long
		
		return t;
	}

	public static <I, O> LearnerType createTTTLearner() {
		LearnerType learner = new LearnerType();
		learner.setClazz(TTTMealyBuilderWrapper.class.getCanonicalName());
		//Map<QName, String> otherAttributes = learner.getOtherAttributes();

		return learner;
	}

	public static <I, O> LearnerType createKVLearner() {
		LearnerType learner = new LearnerType();
		learner.setClazz(KVMealyBuilderWrapper.class.getCanonicalName());
		Map<QName, String> otherAttributes = learner.getOtherAttributes();

		otherAttributes.put(new QName("repeatedCounterexampleEvaluation"),
				Boolean.toString(true));

		return learner;
	}

	public static <I, O> LearnerType createDTLearner() {
		LearnerType learner = new LearnerType();
		learner.setClazz(DTMealyBuilderWrapper.class.getCanonicalName());
		Map<QName, String> otherAttributes = learner.getOtherAttributes();
		otherAttributes.put(new QName("localSuffixFinder"),
				LocalSuffixFinders.RIVEST_SCHAPIRE.toString());
		otherAttributes.put(new QName("repeatedCounterexampleEvaluation"),
				Boolean.toString(true));

		return learner;
	}

	private static <O> MqOracleType createCacheOracle(String desc,
			Map<O, O> mapping) {
		MqOracleType oracle = new MqOracleType();
		oracle.setDesc(desc);
		oracle.setOracle(MembershipOracle.CacheOracle.toString());
		if (mapping != null) {
			PrefixClosureMapping preMap = new PrefixClosureMapping();
			List<Entry> entries = preMap.getEntries();
			for (Map.Entry<O, O> entry : mapping.entrySet()) {
				Entry e = new Entry();
				e.setKey(entry.getKey().toString());
				e.setValue(entry.getValue().toString());
				entries.add(e);
			}
		}
		return oracle;
	}

	private static MqOracleType createCounterOracle(String desc) {
		MqOracleType oracle = new MqOracleType();
		oracle.setDesc(desc);
		oracle.setOracle(MembershipOracle.CounterOracle.toString());
		return oracle;
	}

	public static <I, O> void main(String[] args) throws Exception {
		File workspace = new File("testWorkspace");
		workspace.mkdir();
		File configFile = new File("testWorkspace/activelearning.xml");
		
		LearnerType learner = createTTTLearner();

		// create eq oracle list
		List<EqOracleType> eqs = new ArrayList<>();
		eqs.add(createRWalkO());
		eqs.add(createIWMO());

		List<MqOracleType> mqs = new LinkedList<>();
		mqs.add(createCounterOracle("Pre Cache"));
		mqs.add(createCacheOracle("Cache", null));
		mqs.add(createCounterOracle("Post Cache"));

		try {
			saveToXml(eqs, mqs, learner, workspace, configFile);
		} catch (Exception e) {
			System.err.println("catched....");
			e.printStackTrace();
		}
	}

	public static File saveToXml(List<EqOracleType> eqs, List<MqOracleType> mqs,
			LearnerType learner, File workspace, File configFile)
					throws Exception {
		checkNotNull(eqs);

		LearnSetup al = new LearnSetup();

		EqOraclesType eqtype = new EqOraclesType();
		al.setEqOracles(eqtype);

		List<EqOracleType> eqList = eqtype.getEqOracle();
		eqList.addAll(eqs);

		MqOraclesType mqtype = new MqOraclesType();
		al.setMqOracles(mqtype);
		List<MqOracleType> mqList = mqtype.getMqOracle();
		mqList.addAll(mqs);

		al.setLearner(learner);

		JAXB.marshal(al, configFile);
		return configFile;
	}
}
