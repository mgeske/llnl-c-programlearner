/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.autoSetup.configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXB;

import de.learnlib.util.xml.Entry;
import de.learnlib.util.xml.EqOracleType;
import de.learnlib.util.xml.LearnSetup;
import de.learnlib.util.xml.LearnerType;
import de.learnlib.util.xml.MqOracleType;
import de.learnlib.util.xml.MqOraclesType;
import de.learnlib.util.xml.PrefixClosureMapping;

import de.learnlib.api.LearningAlgorithm.MealyLearner;
import de.learnlib.autoSetup.ConfigurableActiveLearningSetup;
import de.learnlib.autoSetup.MQSetup;
import de.learnlib.autoSetup.ConfigurableActiveLearningSetup.MembershipOracle;
import de.learnlib.builder.eqoracle.EQConfigurator;
import de.learnlib.builder.learner.MealyLearnerBuilder;
import de.learnlib.api.SUL;
import net.automatalib.words.Alphabet;

public class ConfigurationLoader {

	public static <I, O> de.learnlib.autoSetup.ConfigurableActiveLearningSetup<I, O, MealyLearner<I, O>> loadFromXml(
			File config, SUL<I, O> sul, Alphabet<I> sigma, Collection<O> outputs)
			throws FileNotFoundException, ClassNotFoundException,
			NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {

		Reader r = new FileReader(config);
		LearnSetup al = JAXB.unmarshal(r, LearnSetup.class);

		List<? extends EQConfigurator<I, O>> eqs = loadEQs(al);

		MealyLearnerBuilder<I, O, MealyLearner<I, O>> learnerBuilder = loadLearnerBuilder(
				al, sigma);
		List<MQSetup<O>> oracles = loadOracles(al, outputs);
		return new ConfigurableActiveLearningSetup<I, O, MealyLearner<I, O>>(sul, sigma,
				learnerBuilder, oracles, eqs);
	}

	private static <O> List<MQSetup<O>> loadOracles(LearnSetup al,
			Collection<O> outputs) {
		MqOraclesType types = al.getMqOracles();
		List<MQSetup<O>> tuples = new LinkedList<>();

		for (MqOracleType type : types.getMqOracle()) {
			MembershipOracle oracle = MembershipOracle.valueOf(type.getOracle());
			String desc = type.getDesc();

			Map<O, O> prefixMapping = new HashMap<>();
			PrefixClosureMapping mapping = type.getMapping();
			if (mapping != null && outputs != null && !outputs.isEmpty()) {

				Map<String, O> translation = new HashMap<>();
				for (O output : outputs) {
					translation.put(output.toString(), output);
				}

				for (Entry e : mapping.getEntries()) {
					prefixMapping.put(translation.get(e.getKey()),
							translation.get(e.getValue()));
				}
			}

			tuples.add(new MQSetup<>(oracle, desc, prefixMapping));
		}

		return tuples;
	}

	@SuppressWarnings("unchecked")
	private static <I, O> List<? extends EQConfigurator<I, O>> loadEQs(
			LearnSetup al) throws ClassNotFoundException,
			NoSuchMethodException, SecurityException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		List<EQConfigurator<I, O>> eqs = new LinkedList<>();

		for (EqOracleType eqOracle : al.getEqOracles().getEqOracle()) {
			String configuratorClassName = eqOracle.getClazz();
			Class<?> clazz = Class.forName(configuratorClassName);

			Method method = clazz.getMethod("fromXmlElement",
					EqOracleType.class);
			assert (method.getModifiers() & Modifier.STATIC) != 0;
			eqs.add((EQConfigurator<I, O>) method.invoke(null, eqOracle));
		}
		return eqs;
	}

	@SuppressWarnings("unchecked")
	private static <I, O> MealyLearnerBuilder<I, O, MealyLearner<I, O>> loadLearnerBuilder(
			LearnSetup al, Collection<I> sigma) throws ClassNotFoundException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		LearnerType learner = al.getLearner();

		String configuratorClassName = learner.getClazz();
		Class<?> clazz = Class.forName(configuratorClassName);

		Method method = clazz.getMethod("fromXmlElement", LearnerType.class,
				Collection.class);
		assert (method.getModifiers() & Modifier.STATIC) != 0;
		return (MealyLearnerBuilder<I, O, MealyLearner<I, O>>) method.invoke(
				null, learner, sigma);
	}

}
