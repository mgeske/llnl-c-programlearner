/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.autoSetup;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.learnlib.api.EquivalenceOracle.MealyEquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.LearningAlgorithm.MealyLearner;
import de.learnlib.builder.eqoracle.EQConfigurator;
import de.learnlib.builder.learner.MealyLearnerBuilder;
import de.learnlib.builder.mqoracle.MealyCacheOracleWrapper;
import de.learnlib.api.SUL;
import de.learnlib.mealy.MealyUtil;
import de.learnlib.oracles.CounterOracle.MealyCounterOracle;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.MQUtil;
import de.learnlib.oracles.SULOracle;
import de.learnlib.parallelism.DynamicParallelOracleBuilder;
import de.learnlib.parallelism.ParallelOracle;
import de.learnlib.statistics.StatisticOracle;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.incremental.mealy.IncrementalMealyBuilder;
import net.automatalib.incremental.mealy.dag.IncrementalMealyDAGBuilder;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;

public class ConfigurableActiveLearningSetup<I, O, L extends MealyLearner<I, O>> {
	private final LearningAlgorithm.MealyLearner<I, O> learner;
	private final SUL<I, O> sul;
	private final Alphabet<I> sigma;
	private final Map<StatisticOracle<I, Word<O>>, String> statOracles;
	private final List<? extends EQConfigurator<I, O>> eqs;
	private final IncrementalMealyBuilder<I, O> cache;
	private MealyMachine<?, I, ?, O> hyp;
	
	private ParallelOracle<I, Word<O>> parallel;

	// configurable
	protected int hypCounter;
	protected long startTime;
	protected File directory;
	protected String finalResult = null;

	private ConfigurableActiveLearningSetup(SUL<I, O> sul, Alphabet<I> sigma,
			MealyLearnerBuilder<I, O, L> learnerBuilder,
			IncrementalMealyBuilder<I, O> cacheContents,
			List<MQSetup<O>> oracles,
			List<? extends EQConfigurator<I, O>> eqs) {
		checkNotNull(learnerBuilder);

		this.eqs = checkNotNull(eqs);
		this.sul = checkNotNull(sul);
		this.statOracles = new HashMap<>();
		this.sigma = checkNotNull(sigma);
		this.cache = checkNotNull(cacheContents);

		MealyCacheOracleWrapper<I, O> oracle = new MealyCacheOracleWrapper<>(
				this.cache);
		oracle.setNextOracle(new SULOracle<>(sul));

		de.learnlib.api.MembershipOracle<I, Word<O>> sulOracle = createOracleChain(
				oracles, sigma, oracle);
		setUpEQOracles(sulOracle);
		learnerBuilder.setAlphabet(sigma);
		learnerBuilder.setOracle(sulOracle);
		this.learner = learnerBuilder.create();
	}

	public MealyMachine<?, I, ?, O> getHyp() {
		return hyp;
	}

	public enum MembershipOracle {

		CacheOracle {

			@Override
			public <I, O> de.learnlib.api.MembershipOracle<I, Word<O>> create(
					Alphabet<I> a,
					de.learnlib.api.MembershipOracle<I, Word<O>> curOracle,
					String desc) {
				return new MealyCacheOracleWrapper<I, O>().setAlphabet(a)
						.setNextOracle(curOracle).create();
			}
		},
		CounterOracle {

			@Override
			public <I, O> de.learnlib.api.MembershipOracle<I, Word<O>> create(
					Alphabet<I> a,
					de.learnlib.api.MembershipOracle<I, Word<O>> curOracle,
					String desc) {
				return new MealyCounterOracle<>(curOracle, desc);
			}
		};

		public abstract <I, O> de.learnlib.api.MembershipOracle<I, Word<O>> create(
				Alphabet<I> a,
				de.learnlib.api.MembershipOracle<I, Word<O>> curOracle,
				String desc);
	}

	/**
	 * @param sul
	 * @param sigma
	 * @param learnerBuilder
	 * @param oracles
	 * @param eqs
	 */
	public ConfigurableActiveLearningSetup(SUL<I, O> sul, Alphabet<I> sigma,
			MealyLearnerBuilder<I, O, L> learnerBuilder,
			List<MQSetup<O>> oracles,
			List<? extends EQConfigurator<I, O>> eqs) {
		this(sul, sigma, learnerBuilder, new IncrementalMealyDAGBuilder<I, O>(
				sigma), oracles, eqs);
	}

	private void setUpEQOracles(
			de.learnlib.api.MembershipOracle<I, Word<O>> sulOracle) {
		for (EQConfigurator<I, O> e : eqs) {
			e.setSUL(sul, sulOracle);
			e.setAlphabet(sigma);
		}
	}

	public MealyMachine<?, I, ?, O> learn() {
		// compute initial hypothesis
		learner.startLearning(); // learns with cache if existent

		System.err.println("After initial run: " + learner.getHypothesisModel().size() + " states.");
		
		// run for each EQ until no counterexample is found anymore
		for (EQConfigurator<I, O> eq : eqs) {
			System.err.println("Refine with EQ Oracle "
					+ eq.getOracleClass().getSimpleName());
			MealyEquivalenceOracle<I, O> eqOracle = eq.create(learner
					.getHypothesisModel());
			

			DefaultQuery<I, Word<O>> counterexample;
			while ((counterexample = eqOracle.findCounterExample(
					learner.getHypothesisModel(), sigma)) != null) {

				MQUtil.isCounterexample(counterexample,
						learner.getHypothesisModel());
				if (!learner.refineHypothesis(MealyUtil.shortenCounterExample(
						learner.getHypothesisModel(), counterexample))) {
				}
				// refineRuns++;
				// create new custom oracle
				eqOracle = eq.create(learner.getHypothesisModel());
			}
		}
		
		this.parallel.shutdownNow();
		
		return learner.getHypothesisModel();
	}

	/**
	 * Oracles will be chained in order of appearance, putting the first oralce
	 * at the end of the chain and the last at the top.
	 * 
	 * @param oracles
	 *            list of oracles to be chained for the learning process
	 * @param alphabet
	 *            input alphabet to GUL
	 * @return oracle at top level of the oracle chain
	 */
	private de.learnlib.api.MembershipOracle<I, Word<O>> createOracleChain(
			List<MQSetup<O>> oracles, Alphabet<I> alphabet,
			de.learnlib.api.MembershipOracle<I, Word<O>> oracle) {
		
		this.parallel = new DynamicParallelOracleBuilder<>(
				() -> oracle).create();
		
		de.learnlib.api.MembershipOracle<I, Word<O>> curOracle = this.parallel;

		for (MQSetup<O> mq : oracles) {
			curOracle = mq.oracle.create(alphabet, curOracle, mq.desc);

			if (curOracle instanceof StatisticOracle) {
				StatisticOracle<I, Word<O>> o = (StatisticOracle<I, Word<O>>) curOracle;
				statOracles.put(o, mq.desc);
			}
		}
		return curOracle;
	}
}
