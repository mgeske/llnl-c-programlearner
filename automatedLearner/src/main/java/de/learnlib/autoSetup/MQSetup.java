/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.autoSetup;

import java.util.Map;

import de.learnlib.autoSetup.ConfigurableActiveLearningSetup.MembershipOracle;

public class MQSetup<O> {
    public final MembershipOracle oracle;

    public final String desc;

    public final Map<O, O> prefixClosureMapping;

    public MQSetup(MembershipOracle oracle, String description, Map<O, O> prefixClosureMapping) {
        this.oracle = oracle;
        this.desc = description;
        this.prefixClosureMapping = prefixClosureMapping;
    }
}