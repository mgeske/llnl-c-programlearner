/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.cwrapper.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.google.common.base.Splitter;

import de.learnlib.api.LearningAlgorithm.MealyLearner;
import de.learnlib.autoSetup.ConfigurableActiveLearningSetup;
import de.learnlib.autoSetup.configuration.ConfigurationLoader;
import de.learnlib.cwrapper.sul.RERS_C_SUL;
import de.learnlib.cwrapper.util.CustomAutomatonAnalyzer;
import de.learnlib.util.xml.MealyXMLSerializer;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.FastMealy;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.SimpleAlphabet;

public class Main {
	private final ConfigurableActiveLearningSetup<String, String, MealyLearner<String, String>> learner;
	private RERS_C_SUL sul;
	private final Path dir;
	private final String probName;

	public Main(String problemName, File problemFile, Alphabet<String> sigma,
			File config) throws Exception {
		this.dir = problemFile.getParentFile().toPath();
		;
		this.probName = problemName;
		Path workingDir = Files.createTempDirectory("learnedPrograms");

		sul = new RERS_C_SUL(problemFile, problemName, workingDir, sigma);

		learner = ConfigurationLoader.loadFromXml(config, sul, sigma, null);
	}

	public void testProgram(Alphabet<String> sigma) throws Exception {
		MealyMachine<?, String, ?, String> finalHyp = learner.learn();
		sul.destroyWorkingDir();

		System.err.println("Learning terminated");

		GraphDOT.write(finalHyp, sigma, new FileWriter(
				dir.resolve(probName + "beforeTrans.dot").toFile()));

		FastMealy<String, String> mealy = CustomAutomatonAnalyzer
				.transformRERSAutomaton(finalHyp, sigma);
		System.err.println(
				"Removing invalid input transitions and cropping error states.");

		GraphDOT.write(mealy, sigma, new FileWriter(
				dir.resolve(probName + "afterTrans.dot").toFile()));
		System.err
				.println("Inferred automaton has " + mealy.size() + " states.");
		System.err.println("Checking automaton for found errors:");
		Set<Integer> foundErrors = CustomAutomatonAnalyzer
				.findReachableErrors(mealy);
		System.err.println(
				"Found " + foundErrors.size() + " errors: " + foundErrors);

		
		File xmlFile = dir.resolve(probName + "learned.xml").toFile();
		MealyXMLSerializer.toXml(mealy, sigma,
				new BufferedWriter(new FileWriter(
						xmlFile)));
		
		System.err.println("Saved automaton to " + xmlFile);

	}

	public static String removeFileType(String file) {
		List<String> nameParts = new LinkedList<>(
				Splitter.on(".").splitToList(file));
		// remove file type
		nameParts.remove(nameParts.size() - 1);

		String name = "";
		for (String s : nameParts) {
			name = name + s;
		}
		return name;
	}

	public static Alphabet<String> parseAlphabet(String alphabet) {
		return new SimpleAlphabet<>(
				Splitter.on(",").trimResults().splitToList(alphabet));
	}

	/**
	 * args[0] = problem file, relative path args[1] = alphabet of problem
	 * args[2] = config file
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.err.println("Please provide the following information:");
			System.err.println(
					"First argument should be the path to the process that should be learned!");
			System.err.println(
					"Third argument should be input alphabet of the problem.");
			System.err.println(
					"Fourth argument should be the relative path to the active learning configuration file.");
			return;
		}

		boolean error = false;
		File configFile = new File(args[2]);
		File problemFile = new File(args[0]);

		if (!configFile.exists()) {
			System.err.println(
					"Configuration file " + configFile + " does not exist!");
			error = true;
		}
		if (!problemFile.exists()) {
			System.err.println(
					"Program file " + problemFile + " does not exist!");
			error = true;
		}

		if (!problemFile.getName().endsWith(".c")) {
			System.err.println("Program file " + problemFile
					+ " does not seem to be a c file!");
			error = true;
		}

		if (error)
			System.exit(1);
		String probName = removeFileType(problemFile.getName());
		Main tester = new Main(probName, problemFile, parseAlphabet(args[1]),
				configFile);
		tester.testProgram(parseAlphabet(args[1]));
	}
}
