/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.cwrapper.sul;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import de.learnlib.api.SUL;
import de.learnlib.api.SULException;
import de.learnlib.cwrapper.stdioipc.Channel;
import de.learnlib.cwrapper.stdioipc.Output;
import de.learnlib.cwrapper.stdioipc.ProcessWrapper;
import de.learnlib.cwrapper.stdioipc.StreamsClosedException;
import de.learnlib.cwrapper.util.Compiler;

public class WrapperSUL implements SUL<String, String> {
	static Charset CHARSET = Charset.forName("US-ASCII");
	private ProcessWrapper wrapper;
	protected final Path workingDir;
	private long timeout = 100;

	/**
	 * 
	 * @param problemPath
	 * @param problemName
	 * @param workingDir
	 * @param timeout in milliseconds
	 */
	public WrapperSUL(File problemFile, String problemName, Path workingDir, long timeout) {
		this.workingDir = workingDir;
		
		Path problemPath = problemFile.getParentFile().toPath().resolve(problemName);
		
		try {
			Path destPath = workingDir.resolve(problemPath.getFileName());
			Compiler.copyFiles(problemPath, destPath, ".c");
			Compiler.compileSource("gcc", destPath, ".c");
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private WrapperSUL(WrapperSUL other) {
		this.workingDir = other.workingDir;
	}

	@Override
	public void pre() {
		try {
			ProcessBuilder pb = new ProcessBuilder("./a.out")
					.directory(workingDir.toFile()).redirectErrorStream(true);
			pb.environment().clear();
			pb.environment().put("LANG", "C");
			this.wrapper = new ProcessWrapper(pb.start());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void post() {
		wrapper.shutdown();
	}

	@Override
	public WrapperSUL fork() {
		return new WrapperSUL(this);
	}

	@Override
	public String step(String in) throws SULException {
		Output out = null;
		
		try {
			wrapper.write(in.toString());
			out = wrapper.read(timeout, TimeUnit.MILLISECONDS);
		} catch (IOException | InterruptedException e) {
			/*TODO handle not being able to write or read from the process*/
		} catch (StreamsClosedException ex) {
			return "terminated";
		}
		
		
		/*
		 * TODO adapt output handling
		 */
		if(out.getChannel().equals(Channel.STDOUT)){
			return out.getLine();
		} else if (out.getChannel().equals(Channel.STDERR)){
			return out.getLine();
		} else {
			// there was no output at all
			return null;
		}
	}

	@Override
	public boolean canFork() {
		return true;
	}

	public Path getWorkingDir() {
		return workingDir;
	}

	public void destroyWorkingDir() {
		this.workingDir.toFile().delete();
	}
}
