/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.cwrapper.sul;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.List;

import com.google.common.base.Splitter;

import de.learnlib.api.SUL;
import de.learnlib.api.SULException;
import de.learnlib.cwrapper.util.Compiler;
import net.automatalib.words.Alphabet;

public class RERS_C_SUL implements SUL<String, String>{
	
	static Charset CHARSET = Charset.forName("US-ASCII");
	private Writer stdin;
	private BufferedReader stdout;
	private Process cProcess;
	private final Alphabet<String> alphabet;
	protected final Path workingDir;
	private boolean error = false;

	public RERS_C_SUL(File problemFile, String problemName, Path workingDir,
			Alphabet<String> inputs) {
		this.workingDir = workingDir;
		this.alphabet = inputs;
		
		Path problemPath = problemFile.getParentFile().toPath().resolve(problemName);
		
		try {
			Path destPath = workingDir.resolve(problemPath.getFileName());
			Compiler.copyFiles(problemPath, destPath, ".c");
			Compiler.compileSource("gcc", destPath, ".c");
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	private RERS_C_SUL(RERS_C_SUL other) {
		this.workingDir = other.workingDir;
		this.alphabet = other.alphabet;
	}

	@Override
	public void pre() {
		try {
			ProcessBuilder pb = new ProcessBuilder("./a.out")
					.directory(workingDir.toFile()).redirectErrorStream(true);
			pb.environment().clear();
			pb.environment().put("LANG", "C");
			cProcess = pb.start();
			stdin = new OutputStreamWriter(cProcess.getOutputStream(), CHARSET);
			stdout = new BufferedReader(
					new InputStreamReader(cProcess.getInputStream(), CHARSET));
			error = false;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	@Override
	public void post() {
		try {
			stdin.close();
			stdout.close();
		} catch (IOException e) {
			
		}
		cProcess.destroy();
	}

	@Override
	public RERS_C_SUL fork() {
		return new RERS_C_SUL(this);
	}

	@Override
	public String step(String in) throws SULException {
		if (error) {
			return errorHandling(null);
		}
		
		try {
			stdin.write(in + "\n");
			stdin.flush();
		} catch (IOException e) {
			try {				
				return errorHandling(stdout.readLine());
			} catch (IOException ex) {
				throw new SULException(ex);
			}
		}

		String out = null;

		try {
			out = stdout.readLine();
		} catch (IOException e) {
			throw new SULException(e);
		}

		return errorHandling(out);

	}

	private String errorHandling(String out) {
		if (out == null)
			return "error";
		if (out.startsWith("Invalid input:")) {
			List<String> splits = Splitter.on(":").trimResults()
					.omitEmptyStrings().splitToList(out);
			out = "Inv:" + splits.get(splits.size() - 1);
		}
		if (out.contains("Assertion")) {
			error = true;
			int ecStart = out.indexOf("error_");
			if (ecStart != -1) {
				ecStart += 6;
				int ecEnd = ecStart;
				while (ecEnd < out.length()
						&& Character.isDigit(out.charAt(ecEnd))) {
					ecEnd++;
				}
				return out.substring(ecStart - 6, ecEnd);
			}
		}
		return out;
	}

	@Override
	public boolean canFork() {
		return true;
	}

	public Path getWorkingDir() {
		return workingDir;
	}

	public void destroyWorkingDir() {
		this.workingDir.toFile().delete();
	}
	
	public Alphabet<String> getAlphabet() {
		return alphabet;
	}
}
