/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.cwrapper.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Splitter;

import net.automatalib.automata.concepts.InputAlphabetHolder;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.FastMealy;
import net.automatalib.automata.transout.impl.FastMealyState;
import net.automatalib.automata.transout.impl.MealyTransition;
import net.automatalib.util.automata.Automata;
import net.automatalib.util.automata.copy.AutomatonCopyMethod;
import net.automatalib.util.automata.copy.AutomatonLowLevelCopy;
import net.automatalib.words.Alphabet;

public class CustomAutomatonAnalyzer {

	private List<Integer> foundErrors;

	public CustomAutomatonAnalyzer() {
		foundErrors = new ArrayList<>();
	}

	/**
	 * 
	 * @return error numbers found in a RERS program
	 */
	public List<Integer> getFoundErrors() {
		return foundErrors;
	}

	/**
	 * Will shorten the error paths so that only one transition and one state
	 * have to be removed for model checking. Additionally all recognized
	 * invalid inputs are removed (represented as self loops to a state).
	 * The returned automaton is minimal.
	 * 
	 * @param hyp
	 *            learned RERS automaton
	 * @param sigma
	 *            alphabet of RERS problem
	 * @return transformed automaton model of RERS problem
	 */
	public static FastMealy<String, String> transformRERSAutomaton(
			MealyMachine<?, String, ?, String> hyp, Alphabet<String> sigma) {
		FastMealy<String, String> mealy = new FastMealy<>(sigma);
		AutomatonLowLevelCopy.copy(AutomatonCopyMethod.STATE_BY_STATE, hyp,
				sigma, mealy);

		removeGlobalErrorAndTrimInvalid(mealy);

		Automata.invasiveMinimize(mealy, sigma);

		return mealy;
	}

	private static void removeGlobalErrorAndTrimInvalid(
			FastMealy<String, String> mealy) {
		Alphabet<String> sigma = ((InputAlphabetHolder<String>) mealy)
				.getInputAlphabet();
		// remove invalid self edges and redirekt error edges
		for (FastMealyState<String> state : mealy.getStates()) {
			for (String symbol : sigma) {
				for (MealyTransition<FastMealyState<String>, String> t : mealy
						.getTransitions(state, symbol)) {
					String output = t.getOutput();
					if (t.getOutput().contains("error")
							|| t.getOutput().contains("Inv")) {
						mealy.removeTransition(state, symbol, t);
					}
					// if it is an error edge, redirect to so it becomes a self
					// loop
					if (t.getOutput().contains("error_")) {
						mealy.addTransition(state, symbol, state, output);
					}
				}
			}
		}

		// remove unreachable states
		for (FastMealyState<String> state : mealy.getStates()) {
			boolean noTrans = true;
			for (String symbol : sigma) {
				MealyTransition<FastMealyState<String>, String> t = mealy
						.getTransition(state, symbol);
				if (t != null)
					noTrans = false;
			}
			if (noTrans)
				mealy.removeState(state);
		}
	}

	/*
	 * Find all reachable errors (encoded as Integer) in a RERS example. For LTL
	 * verification all identified errors and the transitions reaching them
	 * would have to be removed.
	 */
	@SuppressWarnings("unchecked")
	public static <S, T> Set<Integer> findReachableErrors(
			MealyMachine<S, String, T, String> mealy) {
		Set<S> statesToRemove = new HashSet<>();
		Set<Integer> foundErrors = new HashSet<>();

		for (S state : mealy.getStates()) {

			boolean allReflexive = true;
			boolean allCurrentStateHasNoTransitionForThisInput = true;
			Set<String> outputs = new HashSet<>();
			for (String input : ((InputAlphabetHolder<String>) mealy)
					.getInputAlphabet()) {
				T t = mealy.getTransition(state, input);

				String o = mealy.getOutput(state, input);
				if (o == null)
					continue;
				if (o.contains("error") || o.contains("Current state")) {
					if (o.contains("error_")) {
						List<String> out = Splitter.on("_").trimResults()
								.omitEmptyStrings().splitToList(o);
						foundErrors.add(Integer.parseInt(out.get(1)));
					}
					TransitionHolder<S, T> th = new TransitionHolder<S, T>();
					th.fastmealystate = state;
					th.input = input;
					th.transition = t;
				} else {
					allCurrentStateHasNoTransitionForThisInput = false;
				}

				if (!mealy.getSuccessor(t).equals(state)) {
					allReflexive = false;
					continue;
				}
				outputs.add(mealy.getOutput(state, input));
			}

			if (allReflexive) {
				if (allCurrentStateHasNoTransitionForThisInput) {
					statesToRemove.add(state);
				}
			}
		}

		return foundErrors;
	}
}
