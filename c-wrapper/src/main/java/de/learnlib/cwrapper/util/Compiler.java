/* Copyright (C) 2015 Maren Geske
 * This file is part of c-programlearner, https://bitbucket.org/mgeske/c-programlearner.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.learnlib.cwrapper.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Compiler {

	public static void copyFiles(Path problemPath, Path destPath,
			String extension) throws IOException {
		File file = new File(problemPath + extension);
		File destFile = new File(destPath + extension);
		Files.copy(file.toPath(), destFile.toPath());
	}

	public static void compileSource(String compiler, Path problemPath,
			String extension) throws IOException, InterruptedException {
		Path workingDir = problemPath.getParent();
		File problemFile = new File(problemPath + extension);
		String srcName = problemFile.getName();
		Process p = new ProcessBuilder(compiler, srcName)
				.directory(workingDir.toFile()).inheritIO().start();

		int res = p.waitFor();
		p.destroy();
		if (res != 0)
			throw new IllegalStateException(compiler
					+ " terminated with error code " + res);
	}

}
